import os
from io import StringIO
import pdfminer
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams, LTPage, LTContainer, LTText, LTTextLine
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfparser import PDFParser
from pdfminer.high_level import extract_pages

from cluster_dimension import ClusterDimension


def main() -> None:
    """main function"""
    print(f"PDF miner version: {pdfminer.__version__}")

    home_location_path = os.getenv("HOME")
    print(f"$HOME={home_location_path}")

    if home_location_path is None:
        exit(0)

    doc_path = os.path.join(home_location_path, "test_abrechnungen")
    print(f"doc path: {doc_path}")

    files = os.scandir(doc_path)
    for f in files:
        if not f.is_file():
            continue
        print(f"file: {f.path}")

        extract_text(f.path)
        tables = extract_elements(f.path)

        i = 0
        for t in tables:
            ++i
            print(f"table {i}")
            for p in t:
                print(f"  ({p}): {t[p]}")


def extract_text(file_path: str) -> None:
    """
    Extracts text

    Args:
        file_path (str): path to file
    """
    # https://pdfminersix.readthedocs.io/en/latest/tutorial/composable.html
    print(
        "Extract text ======================================================================================================="
    )
    output_string = StringIO()
    with open(file_path, "rb") as input_file:
        parser = PDFParser(input_file)
        doc = PDFDocument(parser)
        rsrcmgr = PDFResourceManager()
        device = TextConverter(rsrcmgr, output_string, laparams=LAParams())
        interpreter = PDFPageInterpreter(rsrcmgr, device)
        for page in PDFPage.create_pages(doc):
            interpreter.process_page(page)
    print(output_string.getvalue())


def show_container_items(
    container: LTContainer, depth: int, rows: list, cols: list, items: list
):
    """Show elements and sub elements of container

    Args:
        container (LTContainer): container which is inspected
        depth (int): Depth of container to understand the tree structure
    """
    for element in container:
        if isinstance(element, LTTextLine):
            ignore = False
            if element.x0 < 50:
                ignore = True
            text = element.get_text().strip()
            if len(text) == 0:
                ignore = True
            print(
                f"{depth}>element type: {type(element)}, rect: {element.bbox}, text: '{text}', ignore: {ignore}"
            )
            if not ignore:
                rows.append(element.y0)
                cols.append(element.x0)
                items.append(element)
            continue
        if isinstance(element, LTContainer):
            print(f"{depth}>element type: {type(element)}")
            show_container_items(element, depth + 1, rows, cols, items)
            continue
        if isinstance(element, LTText):
            print(f"{depth}>element type: {type(element)}, text: {element.get_text()}")


def convert_page_to_table(page_layout):
    rows = []
    cols = []
    items = []

    # print(f"element type: {type(page_layout)}")
    show_container_items(page_layout, 0, rows, cols, items)

    # print(f"rows ({len(rows)} items): {rows}")
    # print(f"cols ({len(cols)} items): {cols}")
    # print(f"items # : [{len(items)}]")

    clustered_rows = ClusterDimension(rows, 7)
    clustered_cols = ClusterDimension(cols, 25)

    table = dict()
    for i in range(0, len(rows)):
        raw_row = rows[i]
        r = clustered_rows.get_cluster_id(raw_row)
        raw_col = cols[i]
        c = clustered_cols.get_cluster_id(raw_col)
        key = (r, c)
        if not (table.get(key) is None):
            print(f"double entry for: {key} ({raw_row}, {raw_col}), item: {items[i]}")
        table[key] = items[i].get_text().strip()

    # print(f"Number of table cells: {len(table)}")
    return table


def extract_elements(file_path: str) -> None:
    """extract elements

    Args:
        file_path (str): path to file
    """
    # https://pdfminersix.readthedocs.io/en/latest/tutorial/extract_pages.html
    # print(
    #     "Extract elements ======================================================================================================="
    # )

    ret_value = list()
    page_layout: LTPage
    for page_layout in extract_pages(file_path):
        table = convert_page_to_table(page_layout)
        ret_value.append(table)

    return ret_value


main()
