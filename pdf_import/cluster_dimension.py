from scipy.cluster.hierarchy import fcluster, linkage


class ClusterDimension:
    """Clustering positions of a dimension (usually either rows or columns)"""

    __unique_positions: list
    __cluster_assignments: dict

    def __init__(self, positions: list, threshhold: float) -> None:
        unique_positions = list(sorted(dict(map(lambda r: (r, r), positions)).keys()))

        # Perform hierarchical clustering
        hac_positions = linkage([[v] for v in unique_positions], method="ward")

        # Determine the clusters by cutting the dendrogram at a threshold
        clusters_positions = fcluster(hac_positions, t=threshhold, criterion="distance")

        # order the clusters
        unique_cluster_positions = dict(
            map(lambda r: (r, r), clusters_positions)
        ).keys()
        unique_clusters_positions = dict(
            zip(
                unique_cluster_positions,
                list(sorted(unique_cluster_positions)),
            )
        )

        self.__cluster_assignments = dict(
            zip(
                unique_positions,
                [unique_clusters_positions[r] for r in clusters_positions],
            )
        )

        self.__unique_positions = unique_positions

    def get_cluster_id(self, pos):
        """Get ID of cluster for a position value which was used for clustering

        Args:
            pos (float): position value - must have been used for clustering

        Returns:
            int: ID of cluster
        """
        return self.__cluster_assignments[pos]

    def get_clustered_positions(self):
        """Get the values which were used for clustering

        Returns:
            list: all values which were used for clustering
        """
        return self.__unique_positions
